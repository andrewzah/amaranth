require "logger"
LOG = Logger.new(STDOUT)

require "raze"
require "kilt/slang"
require "discordcr"
require "discordcr-middleware"
require "rate_limiter"

# Stock middleware
require "discordcr-middleware/middleware/attribute"
require "discordcr-middleware/middleware/author"
require "discordcr-middleware/middleware/cached_routes"
require "discordcr-middleware/middleware/permissions"
require "discordcr-middleware/middleware/channel"
require "discordcr-middleware/middleware/rate_limiter"

# monkeypatches
module Discord
  module REST
    def create_message(channel_id : UInt64, content : String, embed : Embed? = nil)
      if content != ""
        content = "\u200B" + content
      end
      previous_def(channel_id, content, embed)
    end
  end
end

# Raze monkeypatch
macro render(filename, layout)
  content = render {{filename}}
  render {{layout}}
end

require "./amaranth/context"
require "./amaranth/exceptions"
require "./amaranth/shard_controller"
require "./amaranth/config"
require "./amaranth/util"
require "./amaranth/middlewares/*"
require "./amaranth/plugins/*"
require "./amaranth/server/github.cr"
require "./amaranth/server/guilds.cr"

module Amaranth
  alias AmaranthOptions = Hash(String, String | Int32 | UInt64)
  class_property config : Config = Config.from_json(File.read(File.join("conf", "amaranth.json")))
  class_property rate_limiter = RateLimiter(UInt64).new
  class_property prefix : String = @@config.prefix || ","

  Amaranth.run
end

spawn {
  Raze.run
}

sleep
