# taken from z64's example simple.cr in discord-middleware
# https://github.com/z64/discordcr-middleware/blob/18189855af4e5593cc04c4b2530c4052aae5c996/examples/simple.cr

module Amaranth
  class Common
    def call(payload : Discord::Message, context)
      channel = context[Discord::Client].get_channel(payload.channel_id)
      ctx = Amaranth::Context.new channel

      if id = channel.guild_id
        guild = context[Discord::Client].get_guild(id)
        ctx.guild = guild
      end

      context.put ctx
      yield
    end
  end
end
