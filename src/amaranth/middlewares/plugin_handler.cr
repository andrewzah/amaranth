require "./plugin/*"

module Amaranth
  class_property commands : Hash(String, Discord::Plugin::Command)?
  class_property plugins : Array(Discord::Plugin::Plugin.class)?

  class PluginHandler
    @commands : Hash(String, Discord::Plugin::Command)
    @plugins : Array(Discord::Plugin::Plugin.class)

    def initialize(@plugins)
      @commands = {} of String => Discord::Plugin::Command

      @plugins.each do |plugin|
        plugin.groups.each do |g|
          add(g.name, g.commands)
        end
        add(plugin.commands)
      end

      Amaranth.plugins = @plugins
      Amaranth.commands = @commands
    end

    def call(payload : Discord::Message, context : Discord::Context)
      if @plugins.not_nil!.empty?
        LOG.info("No plugins loaded...")
        yield
      end
      context[Context].plugins = @plugins
      context[Context].commands = @commands

      @plugins.each do |plugin|
        plugin.pre_commands.each do |pre|
          pre.exec.call(payload, context)
        end
      end

      autoclear_message_if_enabled(payload, context)

      result = execute(payload, context, context[Context].ast)
      create_message(context, result)

      yield
    end

    def add(group_name : String, commands : Array(Discord::Plugin::Command))
      commands.each do |cmd|
        cmd.namespace = group_name
        @commands.not_nil!["#{group_name}.#{cmd.name}"] = cmd
      end
    end

    def add(commands : Array(Discord::Plugin::Command))
      commands.each do |cmd|
        cmd.namespace = "none"
        @commands.not_nil!["#{cmd.name}"] = cmd
      end
    end

    private def autoclear_message_if_enabled(payload, context)
      if Amaranth.config.autoclear_msgs == true && context[Context].channel.type == 0 # Text channels only
        channel_id = context[Context].channel.id
        msg_id = payload.id
        context[Discord::Client].delete_message(channel_id, msg_id)
      end
    end

    private def execute(payload, context, ast)
      command, args = get_command_and_args(payload, context, ast)

      range = command.min_args == command.max_args ? "1 arg" : "#{command.min_args}–#{command.max_args} args"
      desc = "#{args.size} args were given, but #{command.name} requires #{range}"
      if args.size < command.min_args
        return Util.error_embed(desc)
      elsif args.size > command.max_args
        return Util.error_embed(desc)
      end

      if resolve_permissions(payload, context, command)
        command.exec.call(payload, args, context)
      else
        Util.error_embed("You don't have the permissions to run this command.")
      end
    end

    private def create_message(context, result)
      id = context[Context].channel.id
      if result.is_a?(String)
        context[Discord::Client].create_message(id, result) if result != ""
      elsif result.is_a?(Discord::Embed)
        context[Discord::Client].create_message(id, "", result)
      elsif result.is_a?(NamedTuple(msg: String, embed: Discord::Embed))
        context[Discord::Client].create_message(id, result[:msg], result[:embed])
      end
    end

    private def resolve_permissions(payload, context, command) : Bool
      permissions = command.permissions
      mw = DiscordMiddleware::Permissions.new(permissions)

      mw.call(payload, context) { true } == true
    end

    private def get_command_and_args(payload, context, ast) : {Discord::Plugin::Command, Array(String)}
      begin
        if ast.is_a?(Parser::Group)
          group = ast.as(Parser::Group)
          group_name = group.name
          name = get_name(payload, context, group.command)
          args = args_to_string(payload, context, group.command.args)

          {@commands.not_nil!["#{group_name}.#{name}"], args}
        else
          command = ast.as(Parser::Command)
          name = get_name(payload, context, command)
          args = args_to_string(payload, context, command.args)

          {@commands.not_nil!["#{name}"], args}
        end
      rescue
        raise Discord::Plugin::MissingException.new("Command or group not found")
      end
    end

    private def get_name(payload, context, command : Parser::Command) : String
      contents = command.name.contents.first

      if contents.is_a?(Parser::Literal)
        contents.as(Parser::Literal).content.to_s
      elsif contents.is_a?(Parser::Command)
        output = execute(payload, context, contents)
        get_content(output)
      else
        "email zah@andrewzah.com"
        # todo possibly error out here
      end
    end

    private def args_to_string(payload, context, arguments : Array(Parser::Argument)) : Array(String)
      args = [] of String
      arguments.each do |arg|
        arg.contents.each do |a|
          if a.is_a?(Parser::Literal)
            args << a.content.to_s
          elsif a.is_a?(Parser::Command)
            output = execute(payload, context, a.as(Parser::Command))
            args << get_content(output)
          end
        end
      end
      args
    end

    private def get_content(output) : String
      if output.is_a?(Discord::Message)
        output.as(Discord::Message).content
      elsif output.is_a?(NamedTuple(msg: String, embed: Discord::Embed))
        output[:msg]
      elsif output.is_a?(Discord::Embed)
        ""
      else
        ""
      end
    end
  end
end
