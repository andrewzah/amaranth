require "./command"

module Discord
  module Plugin
    class Group
      getter commands = [] of Command
      getter name

      def initialize(@name : String)
      end

      def command(name : String, *args, &block : Discord::Message, Array(String), Discord::Context -> CommandReturn) : Command
        cmd = Command.new(name, *args, &block)
        @commands << cmd
        cmd
      end
    end

    class Plugin
      class_getter groups = [] of Group
      class_getter commands = [] of Command
      class_getter pre_commands = [] of PreCommand

      def self.group(name)
        group = Group.new(name)
        with group yield
        @@groups << group
      end

      def self.command(name : String, *args, &block : Discord::Message, Array(String), Discord::Context -> CommandReturn) : Command
        cmd = Command.new(name, *args, &block)
        @@commands << cmd
        cmd
      end

      def self.pre_command(&block : Discord::Message, Discord::Context -> PreCommandReturn) : PreCommand
        pre_cmd = PreCommand.new(&block)
        @@pre_commands << pre_cmd
        pre_cmd
      end
    end
  end
end
