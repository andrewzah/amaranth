module Discord
  module Plugin
    module Config(ConfigType)
      def get_config(file_name : String) : ConfigType
        file = File.join("data", file_name + ".mp")
        unless File.exists?(file)
          File.write(file, ConfigType.new.to_msgpack)
        end
        config = ConfigType.from_msgpack(File.read(file))
      end

      def save_config(file_name : String, config)
        File.write(File.join("data", file_name + ".mp"), config.to_msgpack)
      end
    end
  end
end
