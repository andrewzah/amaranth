module Discord
  module Plugin
    alias CommandReturn = String | Discord::Channel | Discord::Message | Discord::Embed | {msg: String, embed: Discord::Embed} | Nil
    alias PreCommandReturn = Discord::Message | Nil

    class Command
      alias ExecType = Proc(Discord::Message, Array(String), Discord::Context, CommandReturn)
      property name : String
      property min_args : Int32
      property max_args : Int32
      property exec : ExecType
      property permissions : Discord::Permissions
      property description : String
      property namespace : String?

      def to_json(json : JSON::Builder)
        json.object do
          json.field "name", @name
          json.field "namespace", @namespace
          json.field "min_args", @min_args
          json.field "max_args", @max_args
          json.field "permissions", @permissions
          json.field "description", @description
        end
      end

      def initialize(@name, @description, &@exec : ExecType)
        @min_args = 0
        @max_args = 256
        @permissions = Discord::Permissions.new(Discord::Permissions::SendMessages.value)
      end

      def initialize(@name, @description, @min_args : Int32, &@exec : ExecType)
        @max_args = 256
        @permissions = Discord::Permissions.new(SendMessages)
      end

      def initialize(@name, @description, @permissions, &@exec : ExecType)
        @min_args = 0
        @max_args = 256
      end

      def initialize(@name, @description, @min_args : Int32, @max_args : Int32, &@exec : ExecType)
        @permissions = Discord::Permissions.new(SendMessages)
      end

      def initialize(@name, @description, @min_args : Int32, @max_args : Int32, @permissions : Discord::Permissions, &@exec : ExecType)
      end
    end

    class PreCommand
      alias ExecType = Proc(Discord::Message, Discord::Context, PreCommandReturn)
      property exec : ExecType

      def initialize(&@exec : ExecType)
      end
    end
  end
end
