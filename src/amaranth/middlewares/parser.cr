require "./parser/*"

module Amaranth
  class ParserMW
    def initialize(@prefix : String)
    end

    def call(payload : Discord::Message, context : Discord::Context)
      msg = payload.content
      lexer = Parser::Lexer.new(msg, @prefix)
      parser = Parser::Parser.new(lexer)

      ast = nil
      begin
        ast = parser.parse
      rescue e : Parser::ParserError
        context[Discord::Client].create_message(payload.channel_id, e.message || "No err msg.")
      end

      if !ast.nil?
        context[Context].ast = ast
        # context.client.create_message(context.channel.id, "```#{ast.pretty_inspect}```")
        yield
      end
    end
  end
end
