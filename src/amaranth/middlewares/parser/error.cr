module Amaranth
  module Parser
    class ParserError < Exception
      def initialize(x : Int, y : Int, message : String)
        super("[line #{x} character #{y}] #{message}")
      end

      def initialize(lexer : Lexer, message : String)
        initialize(lexer.nice_pos[:line], lexer.nice_pos[:char], message)
      end

      def initialize(pos : Hash(Symbol, Int), message : String)
        raise "pos doesn't have :char or :line" if pos[:char]?.nil? || pos[:line]?.nil?
        initialize(pos[:line], pos[:char], message)
      end
    end

    class DeepParserError < ParserError
      def initialize(x : Int, y : Int, message : String)
        raise("pos: #{x}, #{y} -  [deep error]: #{message}")
      end

      def initialize(lexer : Lexer, message : String)
        initialize(lexer.nice_pos[:line], lexer.nice_pos[:char], message)
      end

      def initialize(pos : Hash(Symbol, Int), message : String)
        raise "pos doesn't have :char or :line" if pos[:char]?.nil? || pos[:line]?.nil?
        initialize(pos[:line], pos[:char], message)
      end
    end
  end
end
