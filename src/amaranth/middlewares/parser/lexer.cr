module Amaranth
  module Parser
    class Lexer
      @str : Array(Char)
      getter string : String
      getter prefix
      property pos : Int32

      def initialize(@string, @prefix : String)
        @str = string.chars
        @pos = 0
        @nice_pos = {:line => 1, :char => 1}
        @finished = false
      end

      getter nice_pos

      getter? finished

      def next_token : Token
        raise ParserError.new(@nice_pos, "Attempted to get token past the end of the string") if @finished

        if @pos == 0
          token = Token.new(:COMMAND_OR_GROUP_START, @prefix)
        else
          case cur_char
          when '.'
            token = Token.new(:GROUP_END, ".")
          when @prefix
            token = Token.new(:COMMAND_START, @prefix)
          when '['
            raise ParserError.new(@nice_pos, "Expected #{@prefix}") unless move_next.to_s == @prefix
            token = Token.new(:COMMAND_START, "\[#{@prefix}")
          when ']'
            token = Token.new(:COMMAND_END, "\]")
          when '{'
            raise ParserError.new(@nice_pos, "Expected #{@prefix}") unless move_next.to_s == @prefix
            token = Token.new(:NORET_COMMAND_START, "\{#{@prefix}")
          when '}'
            token = Token.new(:NORET_COMMAND_END, "\}")
          when '\0'
            token = Token.new(:COMMAND_END, "")
            @finished = true
          when ' '
            token = Token.new(:SEPARATOR, " ")
          when '\n'
            token = Token.new(:SEPARATOR, "\n")
          when '#'
            until cur_char == '\0'
              move_next
            end
            token = Token.new(:LITERAL, "")
          when '"'
            move_next

            content = String.build do |str|
              until {'"', '\0'}.includes? cur_char
                str << read_unescaped

                move_next
              end
            end

            token = Token.new(:LITERAL, content)
          when '`'
            if "``" == peek(2)
              move_next
              move_next

              content = String.build do |str|
                until "```" == peek(3)
                  str << read_unescaped
                  move_next
                end
              end

              token = Token.new(:CODE_BLOCK, content)
            else
              move_next
              content = String.build do |str|
                until {'`', '\0'}.includes? cur_char
                  str << read_unescaped
                  move_next
                end
              end

              token = Token.new(:CODE_SPAN, content)
            end
          else
            content = String.build do |str|
              str << read_unescaped

              until {'.', '[', ']', '{', '}', '$', '"', '\n', ' ', '\0'}.includes? peek_char
                move_next

                str << read_unescaped
              end
            end

            token = Token.new(:LITERAL, content)
          end
        end

        move_next

        token
      end

      private def read_unescaped
        if cur_char == '\\'
          # TODO: handle proper escapes?
          escape_char = move_next

          escape_char
        else
          cur_char
        end
      end

      private def peek(num : Int)
        return '\0' if @pos + num >= @str.size
        @str[@pos + 1..@pos + num]
      end

      private def cur_char
        return '\0' if @pos >= @str.size
        @str[@pos]
      end

      private def peek_char
        return '\0' if @pos + 1 >= @str.size
        @str[@pos + 1]
      end

      private def move_next
        @pos += 1
        @nice_pos[:char] += 1
        ch = cur_char
        if ch == '\n'
          @nice_pos[:char] = 1
          @nice_pos[:line] += 1
        end
        ch
      end
    end
  end
end
