module Amaranth
  module Parser
    record Token,
      type : Symbol, # :COMMAND_START, :COMMAND_END, :NORET_COMMAND_START, :NORET_COMMAND_END, :SEPERATOR, :LITERAL
      content : String

    record Literal,
      content : String

    record Argument,
      contents : Array(Command | Literal)

    record Command,
      name : Argument,
      args : Array(Argument),
      no_return : Bool

    record Group,
      name : String,
      command : Command

    class Parser
      @token : Token?
      @lexer : Lexer?

      def initialize
        @lexer = nil
      end

      def initialize(@lexer : Lexer)
        @token = lexer.next_token
      end

      def reset(lexer : Lexer)
        initialize(lexer)
      end

      def parse : Command | Group
        ensure_lexer
        read_group_or_command
      end

      def parse(string, prefix) : Command
        reset Lexer.new(string, prefix)
        parse
      end

      def ensure_lexer
        raise DeepParserError.new(@lexer.not_nil!, "Attempt to parse without a lexer!") if @lexer.nil?
      end

      def ensure_token
        ensure_lexer
        if @token.nil?
          @token = next_token
          raise DeepParserError.new(@lexer.not_nil!, "Failed getting next token from lexer") if @token.nil?
        end
      end

      private def read_group_or_command
        ensure_token

        if @lexer.not_nil!.string.split.first.includes?('.')
          read_group
        else
          read_command
        end
      end

      private def read_group
        ensure_token

        end_group_token = :GROUP_END
        name = ""

        if @lexer.not_nil!.finished?
          raise ParserError.new(@lexer.not_nil!, "Missing group ending marker (`#{(end_group_token == :GROUP_END) ? "]" : "}"}`)")
        end

        until @token.not_nil!.type == end_group_token
          name += @token.not_nil!.content
          next_token unless @token.not_nil!.type == end_group_token
        end

        prefix = @lexer.not_nil!.prefix.size
        name = name[prefix..-1]

        Group.new(name, read_command)
      end

      private def read_command
        ensure_token

        end_command_token = :INVALID
        end_command_token = :COMMAND_END if @token.not_nil!.type == :COMMAND_END
        end_command_token = :COMMAND_END if @token.not_nil!.type == :GROUP_END
        end_command_token = :COMMAND_END if @token.not_nil!.type == :COMMAND_START
        end_command_token = :COMMAND_END if @token.not_nil!.type == :COMMAND_OR_GROUP_START
        end_command_token = :NORET_COMMAND_END if @token.not_nil!.type == :NORET_COMMAND_START

        if end_command_token == :INVALID
          raise DeepParserError.new(@lexer.not_nil!, "Attempt to read command token at a position that doesn't start with a `[` or `{` token")
        end

        if @lexer.not_nil!.finished?
          raise ParserError.new(@lexer.not_nil!, "Missing command ending marker (`#{(end_command_token == :COMMAND_END) ? "]" : "}"}`)")
        end
        next_token

        args = Array(Argument).new
        until @token.not_nil!.type == end_command_token
          args << read_argument
          next_token unless @token.not_nil!.type == end_command_token
        end

        command = args.shift
        Command.new(command, args, no_return: end_command_token == :NORET_COMMAND_END)
      end

      private def read_argument
        ensure_token
        unless {:COMMAND_START, :NORET_COMMAND_START, :CODE_SPAN, :LITERAL}.includes? @token.not_nil!.type
          raise DeepParserError.new(@lexer.not_nil!, "Attempt to read argument at a position that doesn't start with a command or literal token")
        end

        contents = Array(Command | Literal).new

        until {:SEPARATOR, :COMMAND_END, :NORET_COMMAND_END}.includes? @token.not_nil!.type
          case @token.not_nil!.type
          when :COMMAND_START
            contents << read_command
          when :NORET_COMMAND_START
            contents << read_command
          when :LITERAL
            contents << read_literal
          end

          next_token
        end

        Argument.new(contents)
      end

      private def read_literal
        ensure_token
        unless @token.not_nil!.type == :LITERAL
          raise DeepParserError.new(@lexer.not_nil!, "Attempt to read literal at a position that doesn't start with a literal token")
        end

        content = @token.not_nil!.content
        Literal.new(content)
      end

      private def next_token
        ensure_lexer
        @token = @lexer.not_nil!.next_token
      end
    end
  end
end
