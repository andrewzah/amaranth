# taken from z64's default middlewares
# https://github.com/z64/discordcr-middleware/blob/18189855af4e5593cc04c4b2530c4052aae5c996/src/discordcr-middleware/middleware/error.cr
class DiscordMiddleware::Error
  def initialize(message : String)
    @message = message
    @block = nil
  end

  def initialize(&block : Discord::Context ->)
    @message = nil
    @block = block
  end

  def call(payload : Discord::Message, context : Discord::Context)
    yield
  rescue ex
    if message = @message
      channel_id = payload.channel_id
      message = message.gsub("%exception%", ex.message)
      embed = Amaranth::Util.error_embed(message)
      context[Discord::Client].create_message(channel_id, "", embed)
    end

    @block.try &.call(context)

    raise ex
  end
end
