# adapted from z64's prefix middleware.

class DiscordMiddleware::Prefix
  def initialize(@prefix : String)
  end

  def call(payload : Discord::Message, context : Discord::Context)
    Amaranth.prefix = @prefix

    yield if payload.content.starts_with?(@prefix)
  end
end
