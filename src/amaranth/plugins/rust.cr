module Amaranth
  module Rust
    BASE_RUST_URL = "https://play.rust-lang.org"
    EVAL_URL      = BASE_RUST_URL + "/evaluate.json"
    CRATES_URL    = BASE_RUST_URL + "/meta/crates"
    STABLE_URL    = BASE_RUST_URL + "/meta/version/stable"
    BETA_URL      = BASE_RUST_URL + "/meta/version/beta"
    NIGHTLY_URL   = BASE_RUST_URL + "/meta/version/nightly"

    struct Version
      JSON.mapping(
        version: String,
        hash: String,
        date: String
      )
    end

    struct Crate
      JSON.mapping(
        name: String,
        version: String,
        id: String
      )
    end

    struct Crates
      JSON.mapping(
        crates: Array(Crate)
      )
    end

    struct Request
      JSON.mapping(code: String, version: String, optimize: String)

      def initialize(@code : String, @version : String = "Stable", @optimize : String = "0")
      end
    end

    struct Response
      JSON.mapping(
        result: String,
        error: String?
      )
    end

    def self.crates
      Crates.from_json(HTTP::Client.get(CRATES_URL).body)
    end

    def self.versions
      stable = Version.from_json(HTTP::Client.get(STABLE_URL).body)
      beta = Version.from_json(HTTP::Client.get(BETA_URL).body)
      nightly = Version.from_json(HTTP::Client.get(NIGHTLY_URL).body)

      [stable, beta, nightly]
    end

    def self.version(type)
      case type
      when "stable"
        LOG.info(STABLE_URL)
        Version.from_json(HTTP::Client.get(STABLE_URL).body)
      when "beta"
        Version.from_json(HTTP::Client.get(BETA_URL).body)
      when "nightly"
        Version.from_json(HTTP::Client.get(NIGHTLY_URL).body)
      else
        Version.from_json(HTTP::Client.get(STABLE_URL).body)
      end
    end

    def self.run(code : String, version : String, optimize : String)
      request = Request.new(code, version, optimize)
      response = HTTP::Client.post(
        EVAL_URL,
        HTTP::Headers{
          "Content-Type" => "application/x-www-form-urlencoded",
          "User-Agent"   => "Amaranth, run by <amaranth@andrewzah.com>",
        },
        body: request.to_json)
      raise "request failed:\n```#{response.inspect}```" unless response.success?
      Response.from_json(response.body)
    end
  end

  class RustPlugin < Discord::Plugin::Plugin
    CODE_BLOCK = /```([a-zA-Z]+)\n([\s\S]+?)```/i

    group "rust" do
      desc = <<-DESC
Gets current Rust versions used by #{Rust::BASE_RUST_URL}
Usage: rust.versions
DESC
      command "versions", desc do |msg, args, context|
        versions = Rust.versions

        desc = String.build do |str|
          str << "**Rust versions:**\n"
          versions.each do |v|
            str << "**#{v.version}:** (#{v.date})\n"
          end
        end

        Util.success_embed(desc)
      end

      desc = <<-DESC
Get current crates available. (Top 100 most popular crates on crates.io)
Usage: rust.crates
DESC
      command "crates", desc do |msg, args, context|
        Discord::Embed.new(
          title: "List of available crates on play.rust-lang.org",
          url: Rust::CRATES_URL
        )
      end

      desc = <<-DESC
Check whether a crate is available on #{Rust::BASE_RUST_URL}.
Usage: rust.crate? <crateName>
DESC
      command "crate?", desc do |msg, args, context|
        query = args[0]
        crates = Rust.crates.crates.map { |c| c.name }

        if crates.includes?(query)
          Util.success_embed("Crate #{query} is available.")
        else
          Util.error_embed("Crate #{query} is not available.")
        end
      end

      desc = <<-DESC
Evaluate Rust code on #{Rust::BASE_RUST_URL}
Usage: rust.eval
```
```rust
# Code here.
``
```
DESC
      command "eval", desc do |msg, args, context|
        mode = args[0]? || "stable"
        version = Rust.version(mode)

        if match = CODE_BLOCK.match(msg.content)
          _, _, code = match

          Util.trigger_typing_indicator(context)
          response = Rust.run(code, mode, "0")
          results = if response.error.nil?
                      response.result
                    else
                      response.error
                    end

          content = "```#{results}```"
          content = "(output too long)" if content.size > 1000
          embed = Discord::Embed.new(
            description: "Rust #{version.version}")
          {msg: content, embed: embed}
        else
          "Invalid syntax. Must match: `#{CODE_BLOCK.inspect}`"
        end
      end
    end
  end
end
