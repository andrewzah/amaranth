module Patches
  module Games
    class DOTA
      include Game

      def initialize(@guild_id : UInt64, @channel_id : UInt64)
        @id = 570
        @url = Games.build_steam_url(@id)
        @color = 0x778beb_u32
        @feed_type = FeedTypes::JSON
        @thumbnail_url = "https://i.imgur.com/DBBLY5x.png"
        @interval = ["20:00:00", "20:20:01", "21:00:01"]
        @name = "DotA 2"
      end

      def run(client : Discord::Client, config)
        if news = Games.steam_run(@id.not_nil!, @url)
          if news = Games.check_news(@guild_id, @channel_id, config, news)
            process_news(news).each do |embed|
              sleep Random.rand(2..7)
              client.create_message(@channel_id, "", embed)
            end
          end
        end
      end

      def process_news(news : Array(REST::NewsItem)) : Array(Discord::Embed)
        Games.steam_process_news(news, @color, @thumbnail_url)
      end
    end
  end
end
