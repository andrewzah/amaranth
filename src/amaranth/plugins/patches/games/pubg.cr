module Patches
  module Games
    class PUBG
      include Game

      def initialize(@guild_id : UInt64, @channel_id : UInt64)
        @id = 578080
        @url = Games.build_steam_url(@id)
        @color = 0x574b90_u32
        @feed_type = FeedTypes::XML
        @thumbnail_url = "https://i.imgur.com/lGPmbmp.png"
        @interval = ["09:01:01", "11:01:00", "12:15:00", "16:01:00"]
        @name = "PUBG"
      end

      def run(client : Discord::Client, config)
        if news = Games.steam_run(@id.not_nil!, @url)
          if news = Games.check_news(@guild_id, @channel_id, config, news)
            process_news(news).each do |embed|
              sleep Random.rand(2..7)
              client.create_message(@channel_id, "", embed)
            end
          end
        end
      end

      def remove_markdown_bbcode(string)
        changes = {"[i]"     => "*",
                   "[/i]"    => "*",
                   "[b]"     => "**",
                   "[/b]"    => "**",
                   "[img]"   => "",
                   "[/img]"  => "",
                   "[u]"     => "__",
                   "[/u]"    => "__",
                   "[h1]"    => "",
                   "[/h1]"   => "",
                   "[h2]"    => "",
                   "[/h2]"   => "",
                   "[list]"  => "",
                   "[/list]" => "",
                   "[*]"     => "\n*",
        }

        changes.each do |k, v|
          string = string.gsub(k, v)
        end
        string
      end

      def process_news(news : Array(REST::NewsItem)) : Array(Discord::Embed)
        # grab html url, ignore \r\n, grab anything else after
        image_regex = /([a-zA-Z0-9@:%._\/+~\#=]{2,256}.jpg|[a-zA-Z0-9@:%._\/+~\#=]{2,256}.png)(?:[\r\n]+)(.*)/
        # get href and url for markdown swapping from [url="xyz"][/url]
        url_regex = /\[url=([a-zA-Z0-9:\/.%@_\-\#]*)\]([^\[]*)\[\/url\]/

        to_publish = [] of Discord::Embed
        news.each do |n|
          n.contents = remove_markdown_bbcode(n.contents)
          strings = n.contents.not_nil!.split(" ")

          if match = image_regex.match(strings.first)
            embed_image = Discord::EmbedImage.new(
              url: match[1]
            )

            spaced = strings.join(" ")
            n.contents = spaced[match[1].size..-1]
          end

          n.contents = n.contents.gsub(url_regex) do |all, match|
            "[#{match[2]}](#{match[1]})"
          end

          if n.contents.size > 1999
            n.contents = n.contents[0..1995] + "..."
          end

          footer = Discord::EmbedFooter.new(
            text: n.date.to_s("%A, %B %-d — %l:%M %P")
          )
          thumbnail = Discord::EmbedThumbnail.new(
            url: @thumbnail_url
          )

          if embed_image
            to_publish << Discord::Embed.new(
              title: n.title,
              colour: @color,
              description: n.contents,
              url: n.url,
              footer: footer,
              thumbnail: thumbnail,
              image: embed_image
            )
          else
            to_publish << Discord::Embed.new(
              title: n.title,
              colour: @color,
              description: n.contents,
              url: n.url,
              footer: footer,
              thumbnail: thumbnail,
            )
          end
        end
        to_publish
      end
    end
  end
end
