module Patches
  module Games
    class TF2
      include Game

      def initialize(@guild_id : UInt64, @channel_id : UInt64)
        @id = 440
        @url = Games.build_steam_url(@id)
        @color = 0x778beb_u32
        @feed_type = FeedTypes::JSON
        @thumbnail_url = "https://i.imgur.com/jlTk2CP.png"
        @interval = ["20:30:00", "00:45:00", "01:30:00"]
        @name = "TF2"
      end

      def run(client : Discord::Client, config)
        if news = Games.steam_run(@id.not_nil!, @url)
          if news = Games.check_news(@guild_id, @channel_id, config, news)
            process_news(news).each do |embed|
              sleep Random.rand(2..7)
              client.create_message(@channel_id, "", embed)
            end
          end
        end
      end

      def process_news(news : Array(REST::NewsItem)) : Array(Discord::Embed)
        Games.steam_process_news(news, @color, @thumbnail_url)
      end
    end
  end
end
