module Patches
  module REST
    struct SteamResult
      JSON.mapping(
        app_news: {type: News, key: "appnews"}
      )
    end

    struct News
      JSON.mapping(
        app_id: {type: Int32, key: "appid"},
        news_items: {type: Array(NewsItem), key: "newsitems"},
      )
    end

    struct NewsItem
      JSON.mapping(
        gid: String,
        title: String,
        url: String,
        is_external_url: Bool,
        author: String?,
        contents: String,
        feed_label: {type: String, key: "feedlabel"},
        date: {type: Time, converter: Time::EpochConverter},
        feed_name: {type: String, key: "feedname"},
        feed_type: Int32,
        app_id: {type: Int32, key: "appid"}
      )
    end
  end
end
