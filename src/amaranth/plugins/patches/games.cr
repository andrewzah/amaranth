module Patches
  module Games
    STEAM_BASE_URL   = "http://api.steampowered.com/ISteamNews/GetNewsForApp/"
    NEWS_API_VERSION = "v0002/"
    STEAM_URL        = STEAM_BASE_URL + NEWS_API_VERSION
    PUBLISHERS       = {"steam_updates", "steam_community_announcements"}

    # These are all the games with implemented functionality
    GAMES = {
      "pubg" => 578080,
      "dota" => 570,
      "tf2"  => 440,
    }

    def self.resolve(context : Discord::Context, name : String)
      channel_id = context[Context].channel.id
      if guild = context[Context].guild
        Games.resolve(guild.id, channel_id, name)
      else
        raise GuildMissingException.new "Attempted to call #get_hash but there's no Guild ID. Channel ID: #{channel_id}."
      end
    end

    # Returns a new instance of a game for the Scheduler to run.
    def self.resolve(guild_id : UInt64, channel_id : UInt64, name : String)
      case name
      when "pubg"
        PUBG.new(guild_id, channel_id)
      when "dota"
        DOTA.new(guild_id, channel_id)
      when "tf2"
        TF2.new(guild_id, channel_id)
      else
        raise Exception.new("Game not found. Use patches.list to see available games.")
      end
    end

    def self.build_steam_url(id)
      "#{STEAM_URL}?appid=#{id}&count=5&max_length=1000&format=json"
    end

    def self.steam_run(id : Int32, url : String) : Array(REST::NewsItem) | Nil
      response = HTTP::Client.get(
        url,
        HTTP::Headers{
          "Accept"     => "application/json",
          "User-Agent" => "Amaranth, run by <amaranth@andrewzah.com>",
        }
      )
      json = REST::SteamResult.from_json(response.body)
      news = json.app_news.news_items.select { |n| Games::PUBLISHERS.includes?(n.feed_name) }
    end

    # Checks if the bot has already posted the news
    # to a specific guild/channel
    def self.check_news(guild_id, channel_id, config, news) : Array(REST::NewsItem) | Nil
      to_process = [] of REST::NewsItem
      news.each do |n|
        hash = Util.get_hash(guild_id, channel_id, n.gid)
        if !config.gids[hash]?
          LOG.debug("new gid added: #{n.gid}")
          to_process << n
          config.gids[hash] = 1
        end
      end
      to_process
    end

    def self.remove_html(string)
      changes = {"<br>"     => "\n",
                 "<br/>"    => "\n",
                 "<li>"     => "*",
                 "[img]"    => "",
                 "[/img]"   => "",
                 "[quote]"  => "“",
                 "[/quote]" => "”",
      }

      string = HTML.unescape(string)
      changes.each do |k, v|
        string = string.gsub(k, v)
      end
      string.gsub(/<[^>]*>/, "")
    end

    # Processes the content in order to be readable in Discord
    # e.g. removing HTML elements, etc
    def self.steam_process_news(news : Array(REST::NewsItem), color, thumbnail_url) : Array(Discord::Embed)
      to_publish = [] of Discord::Embed
      news.each do |n|
        footer = Discord::EmbedFooter.new(
          text: n.date.to_s("%A, %B %-d — %l:%M %P")
        )
        thumbnail = Discord::EmbedThumbnail.new(
          url: thumbnail_url
        )
        contents = remove_html(n.contents)
        if contents.size > 1000
          contents = contents[0...1000] + "..."
        end
        to_publish << Discord::Embed.new(
          title: "#{n.title}",
          colour: color,
          description: contents,
          url: n.url,
          footer: footer,
          thumbnail: thumbnail,
        )
      end
      to_publish
    end

    enum FeedTypes
      JSON
      XML
      Other
    end

    module Game
      getter url : String
      getter id : Int32?
      getter color : UInt32
      getter feed_type : FeedTypes
      getter thumbnail_url : String
      getter interval : Array(String)
      getter channel_id : UInt64
      getter name : String

      abstract def initialize(@guild_id, @channel_id)
      abstract def run(client : Discord::Client, config)
      abstract def process_news(news : Array(NewsItem))
    end
  end
end

require "./games/*"
