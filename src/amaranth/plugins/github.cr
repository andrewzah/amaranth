require "msgpack"

module Amaranth
  module Github
    class Config
      MessagePack.mapping({
        links: Hash(String, Array(UInt64)),
      })

      def initialize
        @links = Hash(String, Array(UInt64)).new
      end
    end
  end

  class GithubPlugin < Discord::Plugin::Plugin
    extend Discord::Plugin::Config(Github::Config)

    group "github" do
      moderatorPerms = Discord::Permissions.flags(ManageChannels, ManageGuild)

      desc = <<-DESC
Enable watching a github repo on this channel.
Usage: github.add azah/amaranth
DESC
      command "add", desc, moderatorPerms do |msg, args, context|
        config = get_config("github")
        repo = args[0]

        config.links[repo] ||= [] of UInt64
        config.links[repo] << msg.channel_id
        save_config("github", config)

        Util.success_embed("Added repo **#{repo}**!")
      end

      desc = <<-DESC
List enabled github repos on this channel.
Usage: github.list
DESC
      command "list", desc do |msg, args, context|
        config = get_config("github")
        links = [] of String

        config.links.each do |name, ids|
          ids.each do |id|
            links << name if id == msg.channel_id
          end
        end

        if links.size > 0
          desc = String.build do |str|
            str << "**Enabled Repos:**\n"
            links.each do |name|
              str.puts "* #{name}\n"
            end
          end
          Util.success_embed(desc)
        else
          desc = "No repos enabled. Use #{Amaranth.prefix}github.add to enable one."
          Util.success_embed(desc)
        end
      end

      desc = <<-DESC
Remove a watch github repo from this channel.
Usage: github.remove azah/amaranth
DESC
      command "remove", desc do |msg, args, context|
        config = get_config("github")
        repo = args[0]
        channel_id = msg.channel_id

        if config.links[repo].includes?(channel_id)
          LOG.info(channel_id)
          LOG.info(repo.to_s)
          config.links[repo] = config.links[repo].reject { |id| channel_id == id }
          save_config("github", config)
          Util.success_embed("Removed repo **#{repo}**!")
        else
          Util.error_embed("Repo **#{repo}** was not found for this channel.")
        end
      end
    end
  end
end
