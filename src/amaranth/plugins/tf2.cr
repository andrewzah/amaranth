module Amaranth
  module TF2
    BASE_API_URL = "https://wiki.teamfortress.com/w/api.php"
    PARAMETERS = ["?action=query", "&prop=revisions", "&rvprop=content", "&rvsection=0", "&format=json", "&formatversion=2"]
    BASE_WIKI_URL = "https://wiki.teamfortress.com/wiki/"

    struct Response
      JSON.mapping( query: Query )
    end

    struct Query
      JSON.mapping( pages: Array(Page) )
    end

    struct Page
      JSON.mapping(
        id: { key: "pageid", type: Int32? },
        ns: Int32,
        title: String,
        revisions: Array(Revision)?
      )
    end

    struct Revision
      JSON.mapping(
        format: { key: "contentformat", type: String },
        model: { key: "contentmodel", type: String },
        content: String
      )
    end

    def self.parse_links(string)
      changes = {"'''"     => "**",
                 "'''"    => "**",
      }
      internal_link_regex = /(?:\[\[([^\|\]]*))\|([^\|\]]*)\]\]|(?:\[\[([^\]]*)\]\])/
      external_link_regex = /\[([^ ]*) ([^\]]*)\]/

      string = string.gsub(internal_link_regex) do |string, match|
        if two = match[2]?
          two
        elsif three = match[3]?
          three
        end
      end

      string = string.gsub(external_link_regex) do |string, match|
        link = match[1]?
        text = match[2]?

        if link && text
          "#{text}: #{link}"
        else
          string
        end
      end

      changes.each do |k, v|
        string = string.gsub(k, v)
      end

      string
    end

    def self.parse_content(response)
      if revisions = response.query.pages[0].revisions
        content = revisions[0].content
      else
        raise Exception.new "No content found."
      end

      if matches = content.match(/({{.*}})(.*)/m)
        parsed_links = self.parse_links(matches[2])
        HTML.unescape(parsed_links)
      else
        raise Exception.new "No content found."
      end
    end

    def self.sanitize_args(args)
      args = args.map(&.capitalize)

      if args.size > 1
        args.join("%20")
      else
        args.first
      end
    end

    def self.build_api_url(args)
      params = PARAMETERS.join("")
      url = BASE_API_URL + params + "&titles=#{args}"
    end

    def self.build_wiki_url(args)
      url = BASE_WIKI_URL + args
    end

    def self.run(url)
      response = HTTP::Client.get(
        url,
        HTTP::Headers{ "User-Agent"   => "Amaranth, run by <amaranth@andrewzah.com>", }
      )
      raise "request failed:\n```#{response.inspect}```" unless response.success?
      Response.from_json(response.body)
    end
  end

  desc = <<-DESC
Get information from the TF2 wiki. Takes a title.
Usage: tf2.wiki Rocket Launcher
DESC
  class TF2Plugin < Discord::Plugin::Plugin
    group "tf2" do
      command "wiki", desc do |payload, args, context|
        args = TF2.sanitize_args(args)
        url = TF2.build_api_url(args)
        wiki_url = TF2.build_wiki_url(args)

        Util.trigger_typing_indicator(context)
        response = TF2.run(url)
        content = TF2.parse_content(response)

        Util.success_embed(**{
          title: response.query.pages[0].title,
          description: content,
          url: wiki_url,
        })
      end
    end
  end
end

