require "../middlewares/plugin_handler"

module Amaranth
  class UtilPlugin < Discord::Plugin::Plugin
    allPerms = Discord::Permissions::All
    desc = <<-DESC
Echoes whatever you input.
Usage: echo 1 2 3
DESC
    command "echo", desc do |msg, args, context|
      args.join(" ")
    end

    desc = <<-DESC
Displays general info or specific info about a command.
Usage: help
Usage: help echo
DESC
    command "help", desc do |payload, args, context|
      if args.size == 0
        desc = String.build do |str|
          str.puts "This bot is maintained by Andrei Zah — @Andrei#8263 / zah@andrewzah.com"
          str.puts "It's written in Crystal using discordcr & discordcr-middleware."
          str.puts "*———*"
          str.puts "Amaranth's website is https://amaranth.andrewzah.com"
          str.puts "Please report issues at: https://github.com/azah/amaranth/issues"
          str.puts "To see a list of commands, use #{Amaranth.prefix}commands."
        end

        embed = Util.success_embed(desc)
      else
        group_name = args[0]
        command_name = args[1]? || ""
        group_name += "." unless command_name == ""
        hash = "#{group_name}#{command_name}"

        puts "here3"
        if command = Amaranth.commands.not_nil!.[hash]?
          embed = Util.success_embed(**{
            title:       hash,
            description: command.description,
          })
        else
          raise Discord::Plugin::MissingException.new("Could not find command #{hash}.")
        end
      end

      Util.handle_dm_response(payload, context, embed)
    end

    group "bot" do
      desc = <<-DESC
Change Amaranth's nickname, local per guild.
Usage: bot.rename <newName>
DESC
      command "rename", desc, allPerms do |msg, args, context|
        if guild = context[Context].guild
          user_id = context[Discord::Client].get_current_user.id

          context[Discord::Client].modify_current_user_nick(guild_id: guild.id,
            nick: args.join(" "))
          ""
        else
          Util.no_guild_error_embed
        end
      end

      desc = <<-DESC
Enable or disable Amaranth automatically deleting messages with Amaranth commands.
Usage: bot.clear_messages true|false
DESC
      perms = Discord::Permissions.flags(Administrator)
      command "auto-clear", desc, 1, 1, perms do |msg, args, context|
        arg = args[0].downcase
        if arg == "true" || arg == "false"
          Amaranth.config.autoclear_msgs = Util.true?(args[0])
          Amaranth.save_config
          Util.success_embed("Auto-clearing of command messages is now set to `#{arg}`.")
        else
          Util.error_embed("Input must be `true` or `false`.")
        end
      end
      desc = <<-DESC
Clear X last messages by Amaranth, up to 100. X defaults to 15.
Usage: clear
Usage: clear 2
DESC
      command "clear", desc do |payload, args, context|
        if args[0]?
          limit = args[0].to_u8
        else
          limit = 15_u8
        end
        id = Amaranth.config.bot_id
        msgs = context[Discord::Client].get_channel_messages(context[Context].channel.id, limit: limit)
        filtered_msgs = msgs.select { |m| m.author.id == id }.map { |m| m.id }

        if filtered_msgs.size >= 2
          context[Discord::Client].bulk_delete_messages(context[Context].channel.id, filtered_msgs)
        else
          filtered_msgs.each do |m|
            sleep 1
            context[Discord::Client].delete_message(context[Context].channel.id, m)
          end
        end
        ""
      end
    end

    desc = <<-DESC
Display Amaranth's commands.
Usage: commands
DESC
    command "commands", desc do |payload, args, context|
      group_cmds = String.build do |str|
        groups = [] of Discord::Plugin::Group
        Amaranth.plugins.not_nil!.each do |plugin|
          groups += plugin.groups
        end

        groups.sort_by { |group| group.name }.each do |g|
          str << "#{g.name}: " + g.commands.map { |c| c.name }.sort.join(", ") + "\n"
        end
      end

      general_cmds = String.build do |str|
        commands = [] of String
        Amaranth.plugins.not_nil!.each do |plugin|
          commands += plugin.commands.map { |cmd| cmd.name }
        end
        str << commands.sort.join(", ")
      end

      result = String.build do |str|
        str << "**Group Commands**:\n"
        str << "```"
        str << group_cmds
        str << "```"
        str << "**General Commands**:\n"
        str << "```"
        str << general_cmds
        str << "```"
        str << "To see more detailed information, use #{Amaranth.prefix}help <command>"
      end

      dm_channel = context[Discord::Client].create_dm(payload.author.id)
      context[Discord::Client].create_message(dm_channel.id, "", Util.success_embed(result))
    end

    desc = <<-DESC
Rename channel. Requires `ManageChannels` permission.
Usage: chan.rename <newName>
DESC
    group "chan" do
      renamePerms = Discord::Permissions.flags(ManageChannels)
      command "rename", desc, renamePerms do |msg, args, context|
        context[Discord::Client].modify_channel(context[Context].channel.id, args[0])
      end
    end

    desc = <<-DESC
List info about Amaranth's discord shards.
Usage: shards.info
DESC
    group "info" do
      command "shards", desc do |msg, args, context|
        result = String.build do |str|
          str.puts "**Shards**"
          str << "Amount: **#{Amaranth.shards.size}**\n\n"

          Amaranth.shards.each do |id, shard|
            str.puts "##{id} *—*  **#{shard.name}**:"
            str.puts "  Guilds: **#{shard.client.cache.not_nil!.guilds.size}**"
            str << "  Uptime: **#{shard.uptime}**\n\n"
          end
        end

        Util.success_embed(result)
      end
    end
  end
end
