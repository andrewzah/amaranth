require "schedule"
require "msgpack"

require "../middlewares/plugin_handler"
require "./patches/rest"
require "./patches/games"

module Patches
  include Amaranth

  class Config
    MessagePack.mapping({
      gids:          Hash(String, Int32),
      enabled_games: Hash(String, String),
    })

    def initialize
      @gids = {} of String => Int32
      @enabled_games = {} of String => String
    end
  end

  def self.run(game : String)
    run(GAMES[game])
  end

  class Plugin < Discord::Plugin::Plugin
    extend Discord::Plugin::Config(Patches::Config)

    class_property stop = [] of String
    class_property config : Patches::Config

    @@config = get_config("patches")

    def self.init(client)
      @@config.enabled_games.each do |hash, name|
        channel_id, guild_id = Util.get_from_hash(hash).values
        game = Patches::Games.resolve(guild_id, channel_id, name)

        Schedule.every(:minute) do
          if @@stop.includes?(hash)
            @@stop = @@stop.reject { |s| s == hash }
            Schedule.stop
          end
        end

        Schedule.every(:day, game.interval) do
          if @@stop.includes?(hash)
            @@stop = @@stop.reject { |s| s == hash }
            Schedule.stop
          end

          game.run(client, @@config)
          save_config("patches", @@config)
        end
      end
    end

    group "patches" do
      moderatorPerms = Discord::Permissions.flags(ManageChannels)

      # Either clear a specific GID from a channel/guild, or
      # clear all of the GIDs. [For testing purposes...]
      desc = <<-DESC
Clear either a specific post ID or **ALL** post IDs on this channel (be careful)!
Usage: patches.clear [postID]
Usage: patches.clear
DESC
      command "clear", desc, moderatorPerms do |payload, args, context|
        if gid = args[0]?
          hash = Util.get_hash(context, gid)
          if @@config.gids.has_key?(hash)
            @@config.gids.delete(hash)
            save_config("patches", @@config)
            Util.success_embed("Cleared gid #{gid} from config.")
          else
            Util.error_embed("Cleared gid #{gid} from config.")
          end
        else
          @@config.gids = @@config.gids.clear
          save_config("patches", @@config)
          Util.success_embed("Cleared all gids from config.")
        end
      end

      desc = <<-DESC
Enable watching a game on this channel.
Usage: patches.enable <game>
Note: Game must be one of the implemented games, see the list via patches.games
DESC
      command "enable", desc, moderatorPerms do |payload, args, context|
        name = args[0]
        hash = Util.get_hash(context, name)

        if !@@config.enabled_games[hash]?
          if game = Patches::Games.resolve(context, name)
            @@config.enabled_games[hash] = name
            save_config("patches", @@config)

            Schedule.after(2.seconds) do
              game.run(context[Discord::Client], @@config)
              save_config("patches", @@config)
            end

            Schedule.every(:minute) do
              if @@stop.includes?(hash)
                @@stop = @@stop.reject { |s| s == hash }
                Schedule.stop
              end
            end

            Schedule.every(:day, game.interval) do
              if @@stop.includes?(hash)
                @@stop = @@stop.reject { |s| s == hash }
                Schedule.stop
              end

              sleep(Random.rand(15..60))
              game.run(context[Discord::Client], @@config)
              save_config("patches", @@config)
            end

            Util.success_embed("Enabled game #{name}!")
          else
            Util.error_embed("Game could not be resolved.")
          end
        else
          Util.error_embed("This game is already enabled.\n Use patches.list to see all enabled games.")
        end
      end

      desc = <<-DESC
Disable watching a game on this channel.
Usage: patches.disable <game>
Note: Game must be one of the enabled games, see the list via patches.list
DESC
      command "disable", desc, moderatorPerms do |payload, args, context|
        name = args[0]
        hash = Util.get_hash(context, name)

        if @@config.enabled_games[hash]?
          @@config.enabled_games.delete(hash)
        else
          if guild = context[Context].guild
            context[Discord::Client].get_guild_channels(guild.id).each do |channel|
              hash = Util.get_hash(guild.id, channel.id, name)
              if @@config.enabled_games[hash]?
                @@config.enabled_games.delete(hash)
              end
            end
          end
        end
        save_config("patches", @@config)

        if !@@stop.includes?(hash)
          @@stop << hash
          Util.success_embed("Stopping game #{name}.")
        else
          Util.error_embed("#{name} already queued to be stopped.")
        end
      end

      desc = <<-DESC
List enabled games on this channel.
Usage: patches.list
DESC
      command "list", desc do |payload, args, context|
        games = @@config.enabled_games.select do |k, v|
          result = Util.get_from_hash(k)
          if guild = context[Context].guild
            result[:guild_id] == guild.id
          else
            Util.no_guild_error_embed
          end
        end

        if games.size != 0
          desc = "**Enabled games**:\n#{games.map { |k, v| v }.join(", ")}"
          Util.success_embed(desc)
        else
          Util.error_embed("No games enabled. Use patches.add to add a game.")
        end
      end

      desc = <<-DESC
List all available games to subscribe to patch notes.
Usage: patches.games
DESC
      command "games", desc do |args, context|
        desc = String.build do |str|
          str.puts "**Available games to watch**:"
          Patches::Games::GAMES.each do |k, v|
            str.puts "* #{k}\n"
          end
        end
        Util.success_embed(desc)
      end

      desc = <<-DESC
List all enabled games for every server.
Usage: patches.list_all
DESC
      command "list_all", desc, moderatorPerms do |payload, args, context|
        games = @@config.enabled_games
        if games.size != 0
          ret = [] of NamedTuple(guild_name: String, channel_name: String?, input: String)
          games.each do |k, v|
            tuple = Util.get_from_hash(k)
            ret << {guild_name:   context[Discord::Client].get_guild(tuple[:guild_id]).name,
                    channel_name: context[Discord::Client].get_channel(tuple[:channel_id]).name,
                    input:        tuple[:input]}
          end

          ret.sort_by { |t| t[:guild_name] }
          desc = String.build do |str|
            str << "__ALL enabled games__\n"
            ret.each do |t|
              str << "**#{t[:guild_name]}**##{t[:channel_name]}: #{t[:input]}\n"
            end
          end
          Util.success_embed(desc)
        else
          Util.neutral_embed("No games enabled. Use patches.add to add a game.")
        end
      end

      desc = <<-DESC
List all GIDS. Useful for testing purposes.
Usage: patches.gids
DESC
      command "gids", desc, moderatorPerms do |args, context|
        Util.success_embed(@@config.gids.join("\n"))
      end

      desc = <<-DESC
Manually check for an update, instead of waiting for the scheduled times.
Usage: patches.check [game]
Note: Game must be one of the implemented games, see the list via patches.games
DESC
      command "check", desc do |payload, args, context|
        name = args[0]

        if game = Patches::Games.resolve(context, name)
          game.run(context[Discord::Client], @@config)
          save_config("patches", @@config)
        else
          Util.error_embed("Game not found. Check enabled games with patches.list")
        end
      end

      desc = <<-DESC
Stop every service across every guild.
Usage: patches.stop_all
Note: Do not run this.
DESC
      command "stop_all", desc, moderatorPerms do |args, context|
        @@config.enabled_games.each do |k, v|
          unless @@stop.includes?(k)
            @@stop << k
          end
        end
        @@config.enabled_games = {} of String => String
        save_config("patches", @@config)
        Util.success_embed("Stopping all scheduled tasks.")
      end
    end
  end
end
