require "dota"
require "msgpack"

# Use `DotaPlugin` to avoid naming conflict with `Dota` module.
module DotaPlugin
  include Amaranth

  PLAYER_COLORS = {
    "0":   0x2E6AE6_u32,
    "1":   0x5DE6AD_u32,
    "2":   0xAD00AD_u32,
    "3":   0xDCD90A_u32,
    "4":   0xE66200_u32,
    "128": 0xE67AB0_u32,
    "129": 0x92A440_u32,
    "130": 0x5CC5E0_u32,
    "131": 0x00771F_u32,
    "132": 0x956000_u32,
  }
  OPENDOTA_BASE_URL  = "https://www.opendota.com/"
  OPENDOTA_MATCH_URL = OPENDOTA_BASE_URL + "matches/"
  OPENDOTA_USER_URL  = OPENDOTA_BASE_URL + "players/"

  class Config
    MessagePack.mapping({
      users: Hash(UInt64, User),
    })

    def initialize
      @users = Hash(UInt64, User).new
    end
  end

  class User
    MessagePack.mapping({
      dota_id:    UInt64,
      discord_id: UInt64,
      username:   String,
    })

    def initialize(@dota_id, @discord_id, @username)
    end
  end

  def self.format_match_text(player : Dota::API::Match::Player, match : Dota::API::Match)
    String.build do |str|
      str << "**Type**: #{match.game_mode}, "
      str << "**K/D/A**: #{player.kills}/#{player.deaths}/#{player.assists}, "
      str.puts "**GPM/XPM**: #{player.gold_per_min}/#{player.xp_per_min}"
      str << "**Items**: #{player.items}"
    end
  end

  class Plugin < Discord::Plugin::Plugin
    extend Discord::Plugin::Config(DotaPlugin::Config)

    class_property config : DotaPlugin::Config
    @@config = get_config("dota")

    Dota::Dota.configure do |config|
      config.api_key = ENV["DOTA_API_KEY"]
    end

    group "dota" do
      api = Dota::Dota.api
      perms = Discord::Permissions.new(Discord::Permissions::SendMessages.value)

      desc = <<-DESC
Gets latest dota match.
Usage: dota.latest [dotaID] where ID is optional.
You can save your ID using dota.add. You can find your ID at opendota.com.
DESC
      command "latest", desc, 0, 1, perms do |msg, args, context|
        if args.size == 0
          if user = config.users[msg.author.id]?
            dota_id = user.dota_id
          else
            next Util.error_embed("No associated ID found. Use dota.add <id>.")
          end
        elsif args.size == 1
          dota_id = args[0].to_u64
        end

        context[Discord::Client].trigger_typing_indicator(context[Context].channel.id)

        begin
          basicMatch = api.last_match(dota_id)
          match = api.match(basicMatch.match_id)

          if players = match.players
            player = players.select { |v| v.account_id == dota_id }.first
            hero = api.hero(player.hero_id)
          else
            raise Exception.new("No players found in this match.")
          end

          if player
            text = DotaPlugin.format_match_text(player, match)
            url = "#{DotaPlugin::OPENDOTA_MATCH_URL}#{match.match_id}"
            icon_url = hero.image_url(:lg)

            embed = Discord::Embed.new(
              colour: PLAYER_COLORS["#{player.player_slot}"],
              url: url,
              description: text + url,
              thumbnail: Discord::EmbedThumbnail.new(
                url: icon_url,
              )
            )
          end
        rescue ex
          embed = Discord::Embed.new(
            description: ":x: Something went wrong: ```#{ex.message}```"
          )
        end
        embed
      end

      desc = <<-DESC
Save your dota ID, so you can run `dota.latest` without having to input your ID every time.
DESC
      command "add", desc, 1, 1, perms do |msg, args, context|
        discord_id = msg.author.id
        username = msg.author.username
        dota_id = args[0].to_u64

        user = User.new(dota_id, discord_id, username)
        if @@config.users.has_key?(discord_id)
          desc = "#{username} is already in the database. Updated to #{dota_id}."
        else
          desc = "#{username} added to the database with #{dota_id}."
        end

        @@config.users[discord_id] = user
        save_config("dota", @@config)
        Util.success_embed(desc)
      end

      desc = <<-DESC
Remove your stored dota ID, if it exists.
DESC
      command "remove", desc, 0, 0, perms do |msg, args, context|
        discord_id = msg.author.id

        if @@config.users[discord_id]?
          @@config.users.delete(discord_id)
          save_config("dota", @@config)
          embed = Util.success_embed "Successfully removed dota ID."
        else
          embed = Util.error_embed "No ID found this discord account."
        end

        embed
      end
    end
  end
end
