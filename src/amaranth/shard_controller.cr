module Amaranth
  class_getter sc : ShardController = ShardController.new(Discord::Client.new(Amaranth.config.token))

  # a few methods for simplicity
  # e.g. Amaranth.run instead of Amaranth.sc.run
  def self.run(interval = 86400)
    @@sc.run interval
  end

  def self.sharded?
    @@sc.sharded?
  end

  def self.shards
    @@sc.shards
  end

  def self.guild(guild_id : UInt64) : Discord::Guild | Nil
    shard_id = @@sc.calculate_shard_id(guild_id)
    if shard = @@sc.shards[shard_id]?
      shard.client.cache.not_nil!.guilds[guild_id]?
    end
  end

  def self.all_guilds
    guilds = {} of UInt64 => Discord::Guild
    @@sc.shards.each do |id, shard|
      LOG.info(shard.client.cache.not_nil!.guilds.size)
      guilds.merge! shard.client.cache.not_nil!.guilds
    end
    guilds
  end

  def self.create_message(channel_id : UInt64, message : String? = nil, embed : Discord::Embed? = nil)
    @@sc.shards[0].client.create_message(channel_id, message, embed)
  end

  class Shard
    getter client : Discord::Client
    getter name : String
    getter created_at : Time

    ready : Bool = false
    new : Bool = true

    def ready?
      @ready
    end

    def new
      @new
    end

    def uptime
      span = Time.now - @created_at
      s = span.seconds
      m = span.minutes
      h = span.hours
      d = span.days

      "#{d}d #{h}h, #{m}m #{s}s"
    end

    protected def set_new(bool : Bool)
      @new = bool
    end

    protected def set_ready(bool : Bool)
      @ready = bool
    end

    def initialize(@client : Discord::Client, @name, @created_at)
    end
  end

  class ShardController
    property shards : Hash(Int32, Shard) = {} of Int32 => Shard
    property total_shards : Int32 = 1
    property shard_naming_scheme : Array(String)

    # This client won't #run, but lets us query
    # Discord's GET /gateway/bot endpoint
    def initialize(@auth_only_client : Discord::Client)
      @shard_naming_scheme = File.read_lines(File.join("assets", "shard_naming_scheme.txt"))
    end

    # Formalize Discord's (id >> 22) % @total_shards fn
    def calculate_shard_id(guild_id : UInt64) : UInt64
      (guild_id >> 22) % @total_shards
    end

    def random_name
      Random.new
      taken_names = @shards.map { |id, shard| shard.name }
      available_names = @shard_naming_scheme.reject { |n| taken_names.includes?(n) }
      num = (Random.rand*available_names.size).to_i32
      available_names[num]
    end

    # For logic down the line to check whether
    # or not more than 1 client is running.
    def sharded?
      @shards.size > 1
    end

    # This aims to programmatically check and run
    # the recommended number of shards in their own
    # fibers.
    def run(interval = 86400)
      spawn {
        loop {
          @total_shards = @auth_only_client.get_gateway_bot.shards
          LOG.debug("shards size: #{@shards.size}")
          LOG.debug("total_shards: #{@total_shards}")

          if @shards.size == 0
            initialize_shards
          elsif @total_shards > @shards.size
            grow_shards
          end

          sleep interval
        }
      }
    end

    # Runs on loadup.
    def initialize_shards
      (0...@total_shards).each do |id|
        LOG.info("Creating initial shard #{id}/#{@total_shards}")
        @shards[id] = self.create_shard(id, @total_shards)
        @shards[id].set_new false

        spawn {
          LOG.info("Running inital shard #{id}/#{@total_shards} -- #{@shards[id].name}")
          @shards[id].client.run
        }
      end
    end

    # Runs if @@shards increases during uptime.
    def grow_shards
      LOG.info("Shard total has increased. Recreating clients")
      new_shards = {} of Int32 => Shard

      (0...@total_shards).each do |id|
        LOG.info("\n\n")
        new_shards[id] = self.create_shard(id, @total_shards)
        LOG.info("Created new shard #{id}/#{@total_shards} -- #{new_shards[id].name}")

        spawn {
          LOG.info("Running new shard #{id}/#{@total_shards} -- #{new_shards[id].name}")
          new_shards[id].client.run
        }

        new_shards[id].client.on_ready do
          new_shards[id].set_ready true
        end

        disconnect_shards_if_safe(new_shards)
        sleep 10
      end

      LOG.info("---")
      @shards.each do |id, shard|
        shard.set_new false
        LOG.info("OLD: ##{id} -- #{shard.name}")
      end
      @shards = new_shards
      @shards.each do |id, shard|
        shard.set_new false
        LOG.info("NEW: ##{id} -- #{shard.name}")
      end
    end

    # Rolling restart. This checks if the old shards' guild
    # IDs are safely covered by the newly spinning up shards.
    # It listens for @ready? in the shards.
    def disconnect_shards_if_safe(new_shards)
      @shards.each do |id, shard|
        if !shard.new && can_disconnect?(new_shards, id, shard)
          LOG.info("Attemping to close OLD client #{id} -- #{shard.name}")
          @shards[id].client.stop("Recreating shards")
          @shards.delete(id)
        else
          LOG.info("Can't disconnect #{id} yet. -- #{shard.name},#{shard.new}")
        end
        sleep 2
      end
    end

    # (id >> 22) % @total_shards is Discord's formula for
    # picking what shard gets what traffic.
    def can_disconnect?(new_shards, id, shard)
      shard.client.cache.not_nil!.guilds.all? do |id, guild|
        new_shard_id = ((id >> 22) % @total_shards).to_i32
        if new_shards[new_shard_id]?
          new_shards[new_shard_id].ready?
        else
          false
        end
      end
    end

    # All logic pertaining to setting up & tuning a client,
    # packaged in a Shard.
    def create_shard(shard_id : Int32, num_shards : Int32) : Shard
      client = self.build_client(shard_id, num_shards)
      self.set_client_stack(client)
      self.set_client_on_ready(client)

      Shard.new(client,
        random_name,
        Time.now)
    end

    def build_client(shard_id : Int32, num_shards : Int32) : Discord::Client
      client = Discord::Client.new(Amaranth.config.token,
        client_id: Amaranth.config.bot_id,
        shard: {shard_id: shard_id, num_shards: num_shards})
      cache = Discord::Cache.new(client)
      client.cache = cache
      client
    end

    # The heart of the client. Amaranth is built on middlewares.
    def set_client_stack(client : Discord::Client)
      client.on_message_create(
        Common.new,
        DiscordMiddleware::Error.new("%exception%"),
        DiscordMiddleware::Prefix.new(Amaranth.config.prefix),
        ParserMW.new(Amaranth.config.prefix),
        PluginHandler.new(
          [CarcPlugin,
           DotaPlugin::Plugin,
           GithubPlugin,
           Patches::Plugin,
           RustPlugin,
           TF2Plugin,
           UtilPlugin],
        ))
    end

    # Logic that needs to be run *after* ready needs to go here.
    # E.g. Patches spawns fibers for monitoring steam news.
    def set_client_on_ready(client : Discord::Client)
      client.on_ready do
        if string = Amaranth.config.game
          game = Discord::GamePlaying.new(string, 0_i64)
          client.status_update("bot.andrei.blue", game)
        end

        Patches::Plugin.init(client)
      end
    end
  end
end
