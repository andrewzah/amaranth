module Amaranth
  COLORS = {
    :success => 0x218c74_u32,
    :error   => 0xea2027_u32,
    :neutral => 0x6d6c6c_u32,
  }

  module Util
    def self.true?(obj) : Bool
      obj.to_s == "true"
    end

    # Standardize embeds and make plugins a bit cleaner.
    def self.success_embed(desc : String)
      Discord::Embed.new(
        colour: COLORS[:success],
        description: desc
      )
    end

    def self.success_embed(**args)
      named_tuple = {
        colour: COLORS[:success],
      }

      Discord::Embed.new(
        **args.merge(named_tuple)
      )
    end

    def self.error_embed(desc : String)
      Discord::Embed.new(
        colour: COLORS[:error],
        title: "**Error**",
        description: desc
      )
    end

    def self.error_embed(**args)
      named_tuple = {
        colour: COLORS[:success],
        title:  "**Error**",
      }
      Discord::Embed.new(
        **args.merge(named_tuple)
      )
    end

    def self.neutral_embed(desc : String)
      Discord::Embed.new(
        colour: COLORS[:neutral],
        description: desc
      )
    end

    def self.neutral_embed(**args)
      named_tuple = {
        colour: COLORS[:neutral],
      }
      Discord::Embed.new(
        **args.merge(named_tuple)
      )
    end

    def self.no_guild_error_embed
      self.error_embed("This command must be run in a guild.")
    end

    def self.trigger_typing_indicator(context)
      context[Discord::Client].trigger_typing_indicator(context[Context].channel.id)
    end

    # Standardize config storage. Generally, we want
    # it to be demarcated by guild -> channel -> obj.
    def self.get_hash(context : Discord::Context, input : String)
      cid = context[Context].channel.id
      if context[Context].guild
        gid = context[Context].guild.not_nil!.id
        "#{gid}.#{cid}.#{input}"
      else
        raise GuildMissingException.new "Attempted to call #get_hash but there's no Guild ID. Channel ID: #{cid}."
      end
    end

    def self.get_hash(guild_id : UInt64, channel_id : UInt64, input : String)
      "#{guild_id}.#{channel_id}.#{input}"
    end

    def self.get_from_hash(hash : String)
      elems = hash.split(".")
      {
        guild_id:   elems[0].to_u64,
        channel_id: elems[1].to_u64,
        input:      elems[2],
      }
    end

    # Handle if current channel is a DM channel,
    # otherwise create on with the author ID and send.
    def self.handle_dm_response(payload, context, embed)
      case context[Context].channel.type
      when 0.to_u8 # Text
        dm_channel = context[Discord::Client].create_dm(payload.author.id)
        context[Discord::Client].create_message(dm_channel.id, "", embed)
      when 1.to_u8 # DM
        embed
      else
        ""
      end
    end
  end
end
