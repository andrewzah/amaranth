module Amaranth
  class Context
    property guild : Discord::Guild?
    property channel : Discord::Channel
    property ast : Amaranth::Parser::Command? | Amaranth::Parser::Group?
    property plugins : Array(Discord::Plugin::Plugin.class)?
    property commands : Hash(String, Discord::Plugin::Command)?

    def initialize(@channel)
    end
  end
end
