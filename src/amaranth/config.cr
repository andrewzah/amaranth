require "json"

module UInt64Converter
  def self.from_json(value : JSON::PullParser)
    value.read_string.to_u64
  end

  def self.to_json(value, json : JSON::Builder)
    json.string(value.to_s)
  end
end

module Amaranth
  class Config
    JSON.mapping(
      token: String,
      bot_id: {type: UInt64, converter: UInt64Converter},
      owner_id: {type: UInt64, converter: UInt64Converter},
      prefix: String,
      game: String,
      autoclear_msgs: Bool,
      webhook_tokens: Hash(String, String)
    )
  end

  def self.save_config
    file = File.join("conf", "amaranth.json")
    File.write(file, Amaranth.config.to_pretty_json)
  end
end
