require "msgpack"
require "./github/*"

module Amaranth
  module Kemal
    module Github
      class Config
        MessagePack.mapping({
          links: Hash(String, Array(UInt64)),
        })

        def initialize
          @links = Hash(String, Array(UInt64)).new
        end
      end
    end

    module Github
      extend Discord::Plugin::Config(Config)

      macro define_endpoints(name, secret)
      get "/webhooks/#{{{name}}}" do
        config = get_config("github")
        if config.links["#{{{name}}}"]?
          "There are #{config.links[{{name}}].size} repo(s) enabled for #{{{name}}}."
        else
          "There are no links enabled for #{{{name}}}."
        end
      end

      post "/webhooks/#{{{name}}}" do |ctx|
        secret_token = Amaranth.config.webhook_tokens[{{name}}]
        LOG.info secret_token
        Github.process_ctx(ctx, secret_token)
      end
    end

      error 500 do |ctx, ex|
        "ex: #{ex.inspect_with_backtrace}"
      end

      Amaranth.config.webhook_tokens.each do |k, v|
        define_endpoints(k, v)
      end

      get "/webhooks" do
        config = get_config("github")
        String.build do |str|
          str << "<p> There are #{config.links.size} repo(s) enabled."
          config.links.each do |k, v|
            str << "<h2>#{k}</h2>"
            str << "<ul>"
            v.each do |id|
              str << "<li>#{id}</li>"
            end
            str << "</ul>"
          end
        end
      end

      def self.process_ctx(ctx, secret_token)
        config = get_config("github")
        body = ctx.request.body.not_nil!.gets_to_end

        unless verify_signature(body, ctx.request.headers["X-Hub-Signature"], secret_token)
          puts "Failed to verify signature! Ignoring packet."
        end

        event_name = ctx.request.headers["X-GitHub-Event"]
        case event_name
        when "issues"
          event = Github::IssuesEvent.from_json(body)
        when "create"
          event = Github::CreateEvent.from_json(body)
        when "delete"
          event = Github::DeleteEvent.from_json(body)
        when "issue_comment"
          event = Github::IssueCommentEvent.from_json(body)
        when "pull_request"
          event = Github::PullRequestEvent.from_json(body)
        when "push"
          event = Github::PushEvent.from_json(body)
        when "fork"
          event = Github::ForkEvent.from_json(body)
        when "watch"
          event = Github::WatchEvent.from_json(body)
        else
          event = Github::BasicEvent.from_json(body)
        end

        repo_name = event.repository.full_name
        if event.is_a? Github::BasicEvent
          desc = "Got an #{event_name} for repo #{repo_name} that is not supported — ignoring."
          Util.error_embed(desc)
        else
          desc = "**#{event.sender.login}** " + event.to_s
          embed = Discord::Embed.new(
            author: Discord::EmbedAuthor.new(
              name: "#{event.sender.login} — #{repo_name}",
              icon_url: event.sender.avatar_url,
              url: event.html_url
            ),
            timestamp: Time.now,
            colour: Amaranth::COLORS[:success],
            description: desc,
          )
        end

        if links = config.links[repo_name]?
          links.each do |cid|
            Amaranth.create_message(cid, "", embed)
          end
        end
      end
    end
  end
end
