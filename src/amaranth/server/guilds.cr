module Amaranth
  module API
    module Guilds
      module RPC
        class Call
          JSON.mapping(
            command: String,
            args: Array(String)
          )
        end

        class Response(T)
          JSON.mapping(
            data: T
          )

          def initialize(@data)
          end
        end

        class Error
          JSON.mapping(
            key: String,
            value: String
          )

          def initialize(@key, @value)
          end
        end

        class ErrorResponse
          JSON.mapping(
            errors: Array(Error)
          )

          def initialize(@errors)
          end

          def initialize(key : String, val : String)
            @errors = [] of Error
            @errors << Error.new(key, val)
          end

          def append(key : String, val : String)
            @errors << Error.new(key, val)
          end
        end
      end

      ws "/api/v1/socket" do |ws, ctx|
        ws.on_message do |msg|
          LOG.info("Received msg: #{msg}")
          rpc = RPC::Call.from_json(msg)

          case rpc.command
          when "guilds"
            response = self.guilds
          when "guild"
            response = self.guild(rpc.args.first)
          when "commands"
            response = self.commands
          else
            response = RPC::ErrorResponse.new("rpc", "rpc #{rpc.command} not found")
          end

          LOG.info("Sending #{response}")
          ws.send(response.to_json)
        end

        ws.on_close do
          LOG.warn("Socket has been closed.")
        end
      end

      def self.commands
        if commands = Amaranth.commands
          cmdsArray = commands.map { |k, v| v }
          RPC::Response(Array(Discord::Plugin::Command)).new(cmdsArray)
        else
          RPC::ErrorResponse.new("commands", "No commands were found. Contact Andrew.")
        end
      end

      def self.guilds : RPC::Response(Array(Discord::Guild))
        guilds = Amaranth.all_guilds.map { |k, v| v }
        RPC::Response(Array(Discord::Guild)).new(guilds)
      end

      def self.guild(guild_id)
        if guild = Amaranth.guild(guild_id.to_u64)
          RPC::Response(Discord::Guild).new(guild)
        else
          RPC::ErrorResponse.new("guild", "guild id #{guild_id} not found")
        end
      end
    end
  end
end
