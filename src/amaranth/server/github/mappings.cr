module Amaranth
  module Kemal
    module Github
      struct User
        JSON.mapping(
          login: String,
          id: UInt64,
          avatar_url: String,
          html_url: String,
        )
      end

      struct Repository
        JSON.mapping(
          name: String,
          full_name: String,
          html_url: String,
        )
      end

      struct Label
        JSON.mapping(
          name: String
        )
      end

      struct Issue
        JSON.mapping(
          number: Int32,
          title: String,
          html_url: String,
          body: String,
          labels: {type: Array(Label), nilable: true},
          merged: {type: Bool, nilable: true}
        )

        def tiny_issue
          "##{@number} — " + %(**#{@title}**) + (@labels.try &.map { |e| " `[#{e.name}]`" }.join || "") + ")"
        end

        def format_issue
          String.build do |str|
            str << "##{@number} — **#{@title}**\n"
            str << "---\n"
            str << @body
          end
        end
      end

      struct IssueComment
        JSON.mapping(
          html_url: String,
          body: String,
        )
      end

      struct CommitAuthor
        JSON.mapping(
          username: String
        )
      end

      struct Commit
        JSON.mapping(
          id: String,
          message: String,
          author: CommitAuthor,
          committer: CommitAuthor
        )

        def format_commit
          sha = "#{@id[0..6]}: "
          message = "*#{@message.lines[0].strip}*"
          author = " [#{@author.username}]"
          sha + message + author
        end
      end

      struct IssuesEvent
        JSON.mapping(
          action: String,
          issue: Issue,
          repository: Repository,
          assignee: {type: User, nilable: true},
          label: {type: Label, nilable: true},
          sender: User
        )

        def html_url
          issue.html_url
        end

        def to_s
          case @action
          when "opened"
            %(opened issue #{@issue.format_issue})
          when "reopened"
            %(re-opened issue #{@issue.format_issue})
          when "closed"
            %(closed issue #{@issue.format_issue})
          when "edited"
            %(edited issue #{@issue.format_issue})
          when "assigned"
            %(assigned issue #{@issue.tiny_issue} to **#{@assignee.try &.login}**)
          when "unassigned"
            %(unassigned issue #{@issue.tiny_issue} from **#{@assignee.try &.login}**)
          when "labeled"
            %(added label `[#{@label.try &.name}]` to issue #{@issue.tiny_issue})
          when "unlabeled"
            %(removed label `[#{@label.try &.name}]` from issue #{@issue.tiny_issue})
          else
            ""
          end
        end
      end

      struct PullRequestEvent
        JSON.mapping(
          action: String,
          pull_request: Issue, # identical in format
          repository: Repository,
          assignee: {type: User, nilable: true},
          label: {type: Label, nilable: true},
          sender: User
        )

        def html_url
          pull_request.html_url
        end

        def to_s
          case @action
          when "opened"
            %(opened PR #{@pull_request.format_issue})
          when "reopened"
            %(re-opened PR #{@pull_request.format_issue})
          when "closed"
            %(#{@pull_request.merged ? "merged" : "closed"} PR #{@pull_request.format_issue})
          when "edited"
            %(edited PR #{@pull_request.format_issue})
          when "assigned"
            %(assigned PR #{@pull_request.tiny_issue} to **#{@assignee.try &.login}**)
          when "unassigned"
            %(unassigned PR #{@pull_request.tiny_issue} from **#{@assignee.try &.login}**)
          when "labeled"
            %(added label **[#{@label.try &.name}]** to PR #{@pull_request.tiny_issue})
          when "unlabeled"
            %(removed label **[#{@label.try &.name}]** from PR #{@pull_request.tiny_issue})
          when "synchronize"
            %(updated PR #{@pull_request.tiny_issue} with new commits)
          else
            ""
          end
        end
      end

      struct CreateEvent
        JSON.mapping(
          repository: Repository,
          sender: User,
          ref_type: String,
          ref: String
        )

        def html_url
          @repository.html_url
        end

        def to_s
          "created #{@ref_type} **#{@ref}**"
        end
      end

      struct DeleteEvent
        JSON.mapping(
          repository: Repository,
          sender: User,
          ref_type: String,
          ref: String
        )

        def html_url
          ""
        end

        def to_s
          "deleted #{@ref_type} **#{@ref}**"
        end
      end

      struct IssueCommentEvent
        JSON.mapping(
          repository: Repository,
          sender: User,
          issue: Issue,
          comment: IssueComment,
          action: String
        )

        def html_url
          comment.html_url
        end

        def to_s
          if @action == "created"
            participle = "commented"
          else
            participle = @action + " a comment"
          end

          String.build do |str|
            str << "#{participle} on issue #{@issue.tiny_issue}\n"
            str << "---\n"
            str << "#{@comment.body}"
          end
        end
      end

      struct PushEvent
        JSON.mapping(
          repository: Repository,
          sender: User,
          commits: Array(Commit),
          ref: String,
          compare: String,
        )

        def html_url
          @compare
        end

        def to_s
          split_ref = @ref.split("/")
          ctype = split_ref[1]
          name = split_ref[2..-1].join("/")
          case ctype
          when "heads" # branch
            str = "pushed **#{@commits.size}** commit#{@commits.size == 1 ? "" : "s"} to branch **#{name}**\n"
            str += "---\n"
            str += @commits.map { |e| e.format_commit }.join("\n")
            str
          else
            ""
          end
        end
      end

      struct ForkEvent
        JSON.mapping(
          repository: Repository,
          sender: User,
          forkee: Repository
        )

        def html_url
          forkee.html_url
        end

        def to_s
          "forked this repo to **#{@forkee.full_name}**."
        end
      end

      struct WatchEvent
        JSON.mapping(
          repository: Repository,
          sender: User
        )

        def html_url
          sender.html_url
        end

        def to_s
          "starred this repo!"
        end
      end

      struct BasicEvent
        JSON.mapping(
          repository: Repository,
          sender: User
        )

        def to_s
          ""
        end
      end
    end
  end
end
