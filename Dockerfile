FROM alpine:3.7
MAINTAINER Andrew Zah <zah@andrewzah.com>

COPY lib /srv/amaranth/lib
COPY assets/ /srv/amaranth/assets
COPY src/ /srv/amaranth/src

WORKDIR /srv/amaranth
RUN echo http://public.portalier.com/alpine/testing >> /etc/apk/repositories \
  && wget http://public.portalier.com/alpine/julien%40portalier.com-56dab02e.rsa.pub -O /etc/apk/keys/julien@portalier.com-56dab02e.rsa.pub \
  && apk update \
  && apk add --no-cache --virtual .build-crystal libxml2-dev gcc readline-dev gmp-dev crystal shards \
  && apk add openssl-dev libgcc yaml-dev libxml2 libevent gc pcre-dev \
  && crystal build --no-debug --release src/amaranth.cr \
  && apk del --purge .build-crystal

RUN mkdir -p /src/amaranth/data
CMD ["/srv/amaranth/amaranth"]
