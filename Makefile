.PHONY: build

build:
	crystal build src/amaranth.cr --no-debug -o bin/amaranth

release:
	crystal build src/amaranth.cr --release --no-debug -o bin/amaranth

debug:
	crystal build src/amaranth.cr --debug --error-trace -o bin/amaranth

up: build
	bin/amaranth

docker:
	docker-compose build
	docker-compose down
	docker-compose up

deploy: release
	docker-compose build
	docker push andrewzah/amaranth
